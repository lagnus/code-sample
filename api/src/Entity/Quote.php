<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\State\QuoteProvider;
use Symfony\Component\Validator\Constraints as Assert;

/**
* A quote of superfluous magnificence
*/
#[ApiResource(mercure: false, provider: QuoteProvider::class)]
final class Quote
{
    #[Assert\NotBlank]
    private string $quote = '';

    private function __construct(string $quote)
    {
        return $this->setQuote($quote);
    }

    public static function create(string $quote) : self
    {
        return new self($quote);
    }

    public function getQuote(): ?string
    {
        return $this->quote;
    }

    private function setQuote(string $quote): self
    {
         $this->quote = $quote;
         return $this;
    }

}
