<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use App\Entity\Quote;
use ApiPlatform\State\ProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class QuoteProvider implements ProviderInterface
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritDoc}
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|null|object
    {
        $this->auth($context);
        return $this->quotes();
    }

    private function auth(array $context) : void
    {
        if( !isset($context['request'])
            || "TheEssenceOfHeaderlessTokenAuth" != $context['request']->get('token' , false))
        {
            /** @var Request $request */
            $request = $context['request'];
            throw new AuthenticationException('You must use a valid token to surf this API');
        }
    }

    private function quotes(): array
    {
        $limit = 5;
        $quotes = [];
        do {
            $quotes[] = $this->import();
        } while (--$limit > 0);

        return $quotes;
    }

    private function import(): Quote
    {
        $response = $this->client->request('GET', 'http://api.kanye.rest', [
            'headers' => ['accept' => ['application/json']]
        ]);

        $payload = $response->toArray();
        if ($response->getStatusCode() == 200 && !empty($payload)) {
            return Quote::create($payload['quote']);
        }

        throw new \Exception("Did not receive a valid quote response");
    }
}
