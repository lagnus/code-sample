<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\State\QuoteProvider;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\JsonMockResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class QuoteEndpointTest extends ApiTestCase
{
    public function testGetQuotes(): void
    {
        $aliensJc = [
            ["quote" => "Hey Vasquez, have you ever been mistaken for a man?"],
            ["quote" => "What do you mean *they* cut the power? How could they cut the power, man? They're animals!"],
            ["quote" => "Seventeen *days*? Hey man, I don't wanna rain on your parade, but we're not gonna last seventeen hours!"],
            ["quote" => "Knock it off, Hudson."],
            ["quote" => "I say we take off and nuke the entire site from orbit. It's the only way to be sure."],
        ];

        $mockClient = new MockHttpClient(function(string $method, string $url, array $options = []) use ($aliensJc) {
            static $iteration = 0;
            return new JsonMockResponse($aliensJc[$iteration++]);
        });

        $localClient = static::createClient();
        $localClient->getContainer()->set(QuoteProvider::class, new QuoteProvider($mockClient));
        $localClient->setDefaultOptions([
            'headers' => ['accept' => ['application/json']]
        ]);
        $localClient->request('GET', '/quotes.json?token=TheEssenceOfHeaderlessTokenAuth');
        $this->assertResponseIsSuccessful();
        $this->assertJsonEquals($aliensJc);

        $localClient->request('GET', '/quotes.json');
        $this->assertResponseStatusCodeSame(401);
    }


}
