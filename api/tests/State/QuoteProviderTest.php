<?php

namespace App\Tests\State;

use ApiPlatform\Metadata\GetCollection;
use App\Entity\Quote;
use App\State\QuoteProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\JsonMockResponse;
use Symfony\Component\HttpFoundation\Request;

class QuoteProviderTest extends TestCase
{

    private array $sample;
    private Request|MockObject $mockReq;
    private QuoteProvider $provider;

    protected function setUp(): void
    {
        $this->provider = new QuoteProvider(
            new MockHttpClient(function (string $method, string $url, array $options = []) {
                return new JsonMockResponse($this->sample);
            }));

        $this->mockReq = $this->createConfiguredMock(Request::class, ['get' => "TheEssenceOfHeaderlessTokenAuth"]);

        parent::setUp();
    }


    public function testImportedQuotes()
    {
        $this->sample = ["quote" => "What do you mean *they* cut the power? How could they cut the power, man? They're animals!"];

        /** @var Quote[] $result */
        $result = $this->provider->provide(new GetCollection(), [], ['request' => $this->mockReq]);

        $page = 5;
        $this->assertCount($page, $result);
        while (--$page > 0) {
            $this->assertInstanceOf(Quote::class, $result[$page]);
            $this->assertEquals($this->sample['quote'], $result[$page]->getQuote());
        }
    }

    /**
     * @depends testImportedQuotes
     */
    public function testImportedException()
    {
        $this->sample = [];
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Did not receive a valid quote response");
        $this->provider->provide(new GetCollection(), [], ['request' => $this->mockReq]);

    }
}
