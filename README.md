Test App for kanye.rest access using a base api-platform template with packageless/static token implementation
It is overkill using api-platform but time saving considering a fully featured api is ready to go. 

## To get running

* docker-compose up 
* After the images have built and are running navigate to https://localhost/quotes.json?token=TheEssenceOfHeaderlessTokenAuth
* To run tests use: docker compose exec php bin/phpunit
